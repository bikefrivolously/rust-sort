use std::error::Error;

use clap::{Parser, ValueEnum};
use rand::prelude::*;

const LIST_SIZE: u32 = 10000;

#[derive(Parser)]
#[command(author, version, about)]
struct Cli {
    #[arg(value_enum)]
    algo: SortAlgorithm,
}

#[derive(Clone, ValueEnum)]
enum SortAlgorithm {
    BubbleSort,
    SelectionSort
}

fn main() -> Result<(), Box<dyn Error>> {
    let cli = Cli::parse();

    let mut rng = thread_rng();
    let mut nums = Vec::from_iter(1..=LIST_SIZE);

    nums.shuffle(&mut rng);

    println!("Shuffled: {:?}", nums);

    match cli.algo {
        SortAlgorithm::SelectionSort => selection_sort(&mut nums),
        SortAlgorithm::BubbleSort => bubble_sort(&mut nums),
    }

    println!("Sorted: {:?}", nums);

    Ok(())
}

fn selection_sort<T>(list: &mut Vec<T>)
where
    T: PartialOrd,
{
    let mut min_idx;
    let len = list.len();

    for i in 0..len - 1 {
        min_idx = i;
        for j in i + 1..len {
            if list[j] < list[min_idx] {
                min_idx = j;
            }
        }
        if min_idx != i {
            list.swap(i, min_idx);
        }
    }
}

fn bubble_sort<T: PartialOrd>(list: &mut Vec<T>) {
    let len = list.len();
    let mut swapped = true;
    while swapped {
        swapped = false;
        for i in 1..len {
            if list[i] < list[i-1] {
                list.swap(i, i-1);
                swapped = true;
            }
        }
    }
}
