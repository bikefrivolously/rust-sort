# rust-sort
Implementing sort algorithms and learning rust.

## How?
- generate a sequence of integers
- shuffle the sequence
- sort the shuffled sequence using the sort algorithm de jour

## Potential Graphics Libraries
- piet
- raqote
- piston2d
- lyon - no - path tessel
